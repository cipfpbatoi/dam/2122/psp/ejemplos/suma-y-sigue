package es.cipfpbatoi.psp.sumaysigue;

public class SumaYSigue implements Runnable{

    private long iteraciones = 0;
    private long iteracionActual = 0;
    private int contador = 0;

    public SumaYSigue(long iteraciones)
    {
        this.iteraciones = iteraciones;

    }

    public int getContador()
    {
        return contador;
    }

    public void incrementarContador()
    {
        while (iteracionActual++<iteraciones){
            int aux = contador;
            aux = aux + 1;
            contador = aux;
        }
    }

    @Override
    public void run() {

        incrementarContador();

    }
}
