package es.cipfpbatoi.psp.sumaysigue;

public class App {

    public static void main(String[] args) {

        int numeroThreads = 1;
        long iteraciones = 1000000;

        if (args.length == 1){
            try{
                numeroThreads = Integer.parseInt(args[0]);
            } catch (NumberFormatException e){
                System.err.println("Argumento invalido, utilizando el número de hilos por defecto");
                System.exit(1);
            }
        }

        Thread[] threads = new Thread[numeroThreads];
        SumaYSigue sumaYSigue = new SumaYSigue(iteraciones);

        for (int i = 0; i<numeroThreads; i++){
            threads[i] = new Thread(sumaYSigue);
            threads[i].setName("Thread: "+i);
        }

        for ( Thread t : threads ){
                t.start();
        }

        for ( Thread t : threads ){
            try{
                t.join();
            } catch (InterruptedException e){
                System.err.println(t.getName() + " Interrumpido");
            }
        }

        System.out.println("El valor actual de contador es: "+sumaYSigue.getContador());
        System.out.println("El valor que deberia tener contador es: "+iteraciones);



    }


}
